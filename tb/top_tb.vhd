
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.uart_functions.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;

entity tb is
end tb;

architecture behavior of tb is

  -- Component Declaration for the Unit Under Test (UUT)


  --Inputs
  signal clk  : std_logic := '0';
  signal pb_0 : std_logic := '0';
  signal pb_1 : std_logic := '0';
  signal sw_0 : std_logic := '0';
  signal sw_1 : std_logic := '0';
  signal sclk : std_logic := '0';
  signal mosi : std_logic := '0';
  signal cs_n : std_logic := '0';

  --Outputs
  signal led_0   : std_logic;
  signal led_1   : std_logic;
  signal miso    : std_logic;
  signal pcm_out : std_logic_vector(5 downto 0);

  -- Clock period definitions
  constant clk_period : time := 20 ns;

  -- mock spi signals
  signal mock_send         : boolean := false;
  signal mock_force_cs_low : boolean := false;
  signal mock_ready        : boolean;
  signal mock_data         : std_logic_vector(7 downto 0);

  -- uart signals
  signal pwm_pcm_out : std_logic;
  
begin

  -- Instantiate the Unit Under Test (UUT)
  uut : entity work.top port map (
    clk            => clk,
    pb_0           => pb_0,
    pb_1           => pb_1,
    sw_0           => sw_0,
    sw_1           => sw_1,
    led_0          => led_0,
    led_1          => led_1,
    sclk           => sclk,
    mosi           => mosi,
    cs_n           => cs_n,
    miso           => miso,
    pwm_pcm_out    => pwm_pcm_out
    );



  mock_spi : entity work.mock_spi_master
    port map (
      frequency    => 10_000_000,
      cpol         => 0,
      cpha         => 0,
      send         => mock_send,
      force_cs_low => mock_force_cs_low,
      ready        => mock_ready,
      data         => mock_data,
      cs_n         => cs_n,
      sclk         => sclk,
      mosi         => mosi);

  -- Clock process definitions
  clk_process : process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
  end process;



  -- Stimulus process
  stim_proc : process
    procedure send_mock_byte(
      the_data : in std_logic_vector(7 downto 0)
      ) is

    begin
      if not mock_ready then
        wait until mock_ready;
      end if;
      mock_data <= the_data;

      mock_send <= true;
      wait for 0 ps;
      mock_send <= false;
      wait for 0 ps;
    end send_mock_byte;
    
  begin
    -- hold reset state for 100 ns.
    pb_0 <= '0';
    pb_1 <= '0';
    wait for 100 ns;
    pb_0 <= '0';
    pb_1 <= '1';

    wait for clk_period*10;
    for j in 0 to 10000 loop
      for i in 0 to 255 loop
        if miso = '1' then
          send_mock_byte(std_logic_vector(to_unsigned(i, 8)));
        else
          send_mock_byte("00000000");
        end if;
      end loop;
    end loop;
  end process;

end;
