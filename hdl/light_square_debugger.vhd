library ieee;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

use work.fpga_constants.all;
use work.ws2812_data.all;


entity ws2812_debugger is
  port(
    clk     : in std_logic;
    reset_n : in std_logic;

    -- debug inputs
    empty                : in std_logic;
    full                 : in std_logic;
    request_more         : in std_logic;
    last_data_was_zeroes : in std_logic;
    contents_count       : in natural range 0 to ram_depth - 1;

    pcm    : in std_logic_vector(7 downto 0);
    volume : in integer range 0 to 255;

    -- outputs
    light_square_data : out std_logic
    );
end ws2812_debugger;

architecture rtl of ws2812_debugger is
  constant num_leds          : natural := 32;
  constant num_fullness_leds : natural := 8;
  constant num_volume_leds   : natural := 8;
  constant num_pcm_leds      : natural := 8;

  type ws2812_array_t is array (integer range <>) of ws2812_t;
  signal leds                  : ws2812_array_t(0 to num_leds - 1);
  signal volume_leds           : ws2812_array_t(0 to 7);
  signal buffer_fullness_leds  : ws2812_array_t(0 to 7);
  signal pcm_leds              : ws2812_array_t(0 to 7);
  signal buffer_full_empty_led : ws2812_t;
  signal req_new_data_led      : ws2812_t;
  signal all_zeroes_led        : ws2812_t;

  signal led_being_outputted : ws2812_t;
  signal current_led         : integer range 0 to num_leds - 1;
  
begin
  
  ws2812_drv_1 : entity work.ws2812_drv
    generic map (
      num_leds => num_leds
      )
    port map (
      clk         => clk,
      reset_n     => reset_n,
      data_in     => led_being_outputted,
      current_led => current_led,

      data_out => light_square_data
      );

  led_being_outputted <= leds(current_led);

  leds(0 to 7)   <= buffer_fullness_leds;
  leds(8)        <= buffer_full_empty_led;
  leds(9)        <= req_new_data_led;
  leds(10)       <= all_zeroes_led;
  leds(11 to 15) <= (others => (green => 0, red => 0, blue => 0));
  leds(16 to 23) <= volume_leds;
  leds(24 to 31) <= pcm_leds;

  buffer_leds_1 : for i in 0 to 7 generate
    
    debug_buffer_fullness_1 : entity work.debug_buffer_fullness
      generic map(
        num_fullness_leds => 8,
        i                 => i
        )
      port map (
        clk            => clk,
        reset_n        => reset_n,
        contents_count => contents_count,
        the_led        => buffer_fullness_leds(i)
        );
  end generate;

  do_the_volume_debugging : for i in 0 to num_volume_leds - 1 generate
    debug_volume_led_1 : entity work.debug_volume_led
      generic map (
        num_volume_leds => num_volume_leds,
        i               => i
        )
      port map (
        clk     => clk,
        reset_n => reset_n,
        volume  => volume,
        the_led => volume_leds(i)
        );
  end generate;

  do_the_pcm_debugging : for i in 0 to num_pcm_leds - 1 generate
    debug_pcm_led_1 : entity work.debug_pcm_led
      generic map (
        num_pcm_leds => num_pcm_leds,
        i               => i
        )
      port map (
        clk     => clk,
        reset_n => reset_n,
        pcm     => pcm,
        the_led => pcm_leds(i)
        );
  end generate;

  colour_in_empty_full : process (clk) is
  begin
    if rising_edge(clk) then
      if reset_n = '0' then
        null;
      else
        if full = '1' then
          set_ws2812(buffer_full_empty_led, 0, 0, 32);
        elsif empty = '1' then
          set_ws2812(buffer_full_empty_led, 32, 0, 0);
        else
          clear_ws2812(buffer_full_empty_led);
        end if;
      end if;
    end if;
  end process colour_in_empty_full;

  debug_request_more : process (clk) is
  begin
    if rising_edge(clk) then
      if reset_n = '0' then
        null;
      else
        if request_more = '1' then
          set_ws2812(req_new_data_led, 16, 0, 16);
        else
          clear_ws2812(req_new_data_led);
        end if;
      end if;
    end if;
  end process debug_request_more;

  debug_last_data_was_all_Zeroes : process (clk) is
  begin
    if rising_edge(clk) then
      if reset_n = '0' then
        null;
      else
        if last_data_was_zeroes = '1' then
          set_ws2812(all_zeroes_led, 10, 22, 0);
        else
          clear_ws2812(all_zeroes_led);
        end if;
      end if;
    end if;
  end process debug_last_data_was_all_Zeroes;
end rtl;

