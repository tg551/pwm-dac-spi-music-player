library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.fpga_constants.all;


entity circular_queue is
  port (clk     : in std_logic;
        reset_n : in std_logic;

        enqueue        : in  std_logic;
        dequeue        : in  std_logic;
        write_in_data  : in  std_logic_vector(7 downto 0);
        read_out_data  : out std_logic_vector(7 downto 0);
        request_more   : out std_logic;
        empty          : out std_logic;
        full           : out std_logic;
        contents_count : out natural range 0 to ram_depth - 1
        );
end circular_queue;

architecture rtl of circular_queue is
  constant ram_depth_addr_length : integer := integer(ceil(log2(real(ram_depth))));

  --ram signals
  signal write_enable  : std_logic;
  signal read_address  : unsigned(ram_depth_addr_length - 1 downto 0);
  signal write_address : unsigned(ram_depth_addr_length - 1 downto 0);
  signal data_in       : std_logic_vector(ram_width downto 0);
  signal data_out      : std_logic_vector(ram_width downto 0);

  --
  constant start_req_more_count    : positive := integer(circ_buffer_request_thresh * real(ram_depth));
  constant end_req_more_high_count : positive := integer(circ_buffer_stop_request_thresh * real(ram_depth));
  signal full_int                  : std_logic;
  signal empty_int                 : std_logic;
  signal request_more_int          : std_logic;
  signal contents_count_int :  natural range 0 to ram_depth - 1;
begin
  ram_1 : entity work.music_ram
    port map (
      clk           => clk,
      read_address  => read_address,
      write_address => write_address,
      write_enable  => write_enable,
      write_in      => write_in_data,

      read_out => read_out_data);

  calc_request_more : process (clk) is
  begin
    if rising_edge(clk) then
      if reset_n = '0' then
        contents_count_int   <= 0;
        request_more_int <= '0';
      else
        contents_count_int <= natural(to_integer(write_address - read_address));

        if request_more_int = '1' and contents_count_int >= end_req_more_high_count then
          request_more_int <= '0';
        elsif request_more_int = '0' and contents_count_int <= start_req_more_count then
          request_more_int <= '1';
        end if;
      end if;
    end if;
  end process calc_request_more;

  request_more <= request_more_int;

  full_int <= '1' when read_address = write_address + 1
              else '0';

  full <= full_int;
  empty <= empty_int;
  contents_count <= contents_count_int;

  emptying : process (clk) is
  begin
    if rising_edge(clk) then
      if reset_n = '0' then
        empty_int <= '0';
      else
        if read_address = write_address then
          empty_int <= '1';
        else
          empty_int <= '0';
        end if;
      end if;
    end if;
  end process emptying;



  write_enable <= '1' when full_int = '0' and enqueue = '1'
                  else '0';

  --allow a 'read' to be performed, except when there is nothing left to read. 
  dequeue_something : process (clk) is
  begin
    if rising_edge(clk) then
      if reset_n = '0' then
        read_address <= to_unsigned(0, read_address'length);
      else
        if dequeue = '1' and empty_int = '0' then
          read_address <= read_address + 1;
        end if;
      end if;
    end if;
  end process dequeue_something;

  --Allow a write to be performed, except when there is no more room
  update_write_address : process (clk) is
  begin
    if rising_edge(clk) then
      if reset_n = '0' then
        write_address <= to_unsigned(0, write_address'length);
      else
        if enqueue = '1' and full_int = '0' then
          write_address <= write_address + 1;
        end if;
      end if;
    end if;
  end process update_write_address;
end rtl;
