library ieee;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

use work.fpga_constants.all;
use work.ws2812_data.all;


entity debug_pcm_led is
  generic(num_pcm_leds : natural;
          i            : integer);
  port(
    clk     : in std_logic;
    reset_n : in std_logic;

    pcm : in std_logic_vector(7 downto 0);

    the_led : out ws2812_t
    );
end debug_pcm_led;

architecture rtl of debug_pcm_led is
  constant contents_per_led : natural := 255 / num_pcm_leds;
  constant our_limit        : natural := (i * contents_per_led);
  signal the_led_int        : ws2812_t;
begin
  go : process (clk) is
  begin
    if rising_edge(clk) then
      if reset_n = '0' then
        null;
      else
        if 255 - to_integer(unsigned(pcm)) < our_limit then
          --set_ws2812(the_led_int, 8, 0, 4);
          set_ws2812(the_led_int, 255, 255, 255);
        else
          clear_ws2812(the_led_int);
        end if;
      end if;
    end if;
  end process go;

  the_led <= the_led_int;
  
end rtl;
