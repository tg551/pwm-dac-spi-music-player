library ieee;
use ieee.STD_LOGIC_1164.all;

entity input_conditioning is
  port(
    clk     : in std_logic;
    reset_n : in std_logic;
    sw_0    : in std_logic;
    sw_1    : in std_logic;
    pb_0    : in std_logic;
    pb_1    : in std_logic;

    sw_0_db : out std_logic;
    sw_1_db : out std_logic;
    pb_0_db : out std_logic;
    pb_1_db : out std_logic
    );
end input_conditioning;






architecture rtl of input_conditioning is
begin

  debounce_pb_0 : entity work.debounce
    generic map (debounce_time => 10 ms)
    port map (
      clk        => clk,
      reset_n    => reset_n,
      in_signal  => pb_0,
      out_signal => pb_0_db
      );

  debounce_pb_1 : entity work.debounce
    generic map (debounce_time => 10 ms)
    port map (
      clk        => clk,
      reset_n    => reset_n,
      in_signal  => pb_1,
      out_signal => pb_1_db
      );

  debounce_sw_0 : entity work.debounce
    generic map (debounce_time => 10 ms)
    port map (
      clk        => clk,
      reset_n    => reset_n,
      in_signal  => sw_0,
      out_signal => sw_0_db
      );

  debounce_sw_1 : entity work.debounce
    generic map (debounce_time => 10 ms)
    port map (
      clk        => clk,
      reset_n    => reset_n,
      in_signal  => sw_1,
      out_signal => sw_1_db
      );

end rtl;
