
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use ieee.math_real.all;

package fpga_constants is

----------------------------------------------------------------------------------------------
-- Define Main CLK specification
----------------------------------------------------------------------------------------------
  constant fpga_clock_period : time := 20 ns;


----------------------------------------------------------------------------------------------
-- pcm data. Bad names of constants and location of constants.
----------------------------------------------------------------------------------------------
  constant ram_depth : integer := 32768;
  constant ram_width : integer := 8;
  type ram_type is array (0 to ram_depth - 1) of std_logic_vector (ram_width - 1 downto 0);

----------------------------------------------------------------------------------------------
-- circular buffer and spi comms tunables
----------------------------------------------------------------------------------------------
  constant circ_buffer_request_thresh      : real := 0.1;
  constant circ_buffer_stop_request_thresh : real := 0.99;


----------------------------------------------------------------------------------------------
-- spi
----------------------------------------------------------------------------------------------
  constant spi_word_length : positive := 8;

----------------------------------------------------------------------------------------------
-- uart
----------------------------------------------------------------------------------------------
  constant baud_rate : positive := 115200;
  
----------------------------------------------------------------------------------------------
-- functions?!
----------------------------------------------------------------------------------------------
  function zero_to_z(v : in std_logic_vector) return std_logic_vector;
end package;

package body fpga_constants is
  function zero_to_z(v : in std_logic_vector) return std_logic_vector is
    variable ret : std_logic_vector(v'range);
  begin
    ret := v;
    for i in ret'range loop
      if ret(i) = '0' then
        ret(i) := 'Z';
      end if;
    end loop;
    return ret;
  end zero_to_z;
end fpga_constants;
