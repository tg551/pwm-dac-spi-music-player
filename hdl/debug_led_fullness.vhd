library ieee;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

use work.fpga_constants.all;
use work.ws2812_data.all;


entity debug_buffer_fullness is
  generic(num_fullness_leds : natural;
          i                 : integer);
  port(
    clk     : in std_logic;
    reset_n : in std_logic;

    contents_count : in integer range 0 to ram_depth - 1;

    the_led : out ws2812_t
    );
end debug_buffer_fullness;

architecture rtl of debug_buffer_fullness is
  constant contents_per_led : natural := ram_depth / num_fullness_leds;
  constant our_limit : natural := (i * contents_per_led + 2);
  signal the_led_int : ws2812_t;
begin
  go : process (clk) is
  begin
    if rising_edge(clk) then
      if reset_n = '0' then
        null;
      else
        if ram_depth - contents_count < our_limit  then
          set_ws2812(the_led_int, 0, 8, 8);
        else
          clear_ws2812(the_led_int);
        end if;
      end if;
    end if;
  end process go;

  the_led <= the_led_int;
  
end rtl;
