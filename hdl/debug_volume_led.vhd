library ieee;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

use work.fpga_constants.all;
use work.ws2812_data.all;


entity debug_volume_led is
  generic(num_volume_leds : natural;
          i               : integer);
  port(
    clk     : in std_logic;
    reset_n : in std_logic;

    volume : in integer range 0 to 255;

    the_led : out ws2812_t
    );
end debug_volume_led;

architecture rtl of debug_volume_led is
  constant contents_per_led : natural := 255 / num_volume_leds;
  constant our_limit : integer := i * contents_per_led;
  signal the_led_int : ws2812_t;
begin
  go : process (clk) is
  begin
    if rising_edge(clk) then
      if reset_n = '0' then
        null;
      else
        if 255 - volume < our_limit then
          set_ws2812(the_led_int, 0, 9, 0);
        else
          clear_ws2812(the_led_int);
        end if;
      end if;
    end if;
  end process go;

  the_led <= the_led_int;
  
end rtl;
