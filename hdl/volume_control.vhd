
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.NUMERIC_STD.all;
use work.fpga_constants.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity volume_control is
  port(
    clk         : in  std_logic;
    reset_n     : in  std_logic;
    volume_up   : in  std_logic;
    volume_down : in  std_logic;

    volume      : out integer range 0 to 255
    );
end volume_control;

architecture rtl of volume_control is
  signal volume_int : integer range 0 to 255;

  --timing constants
  constant volume_full_range_time : time := 10 sec;

  -- Derived constants
  constant single_volume_change_time : time     := volume_full_range_time / 256;
  constant single_volume_count       : positive := single_volume_change_time / fpga_clock_period;

  signal single_volume_counter : integer range 0 to single_volume_count;
  signal single_volume_count_done : std_logic;
begin

  -- Produce a strobe at the rate we need to implement the overall wait time.
  -- Note that this strobe work continuously whether or not we are requesting a volume change
  counter_for_single_volume_change : process (clk) is
  begin
    if rising_edge(clk) then
      if reset_n = '0' then
        single_volume_counter    <= 0;
        single_volume_count_done <= '0';
      else
        if single_volume_counter >= single_volume_count then
          single_volume_counter    <= 0;
          single_volume_count_done <= '1';
        else
          single_volume_counter    <= single_volume_counter + 1;
          single_volume_count_done <= '0';
        end if;
      end if;
    end if;
  end process counter_for_single_volume_change;

  change_volume : process (clk) is
    
    procedure change_volume is
    begin
      if volume_int < 255 and volume_up = '0' then
        volume_int <= volume_int + 1;
      elsif volume_int > 0 and volume_down = '0' then
        volume_int <= volume_int - 1;
      end if;
    end change_volume;
    
  begin
    if rising_edge(clk) then
      if reset_n = '0' then
        volume_int <= 200;
      else
        if single_volume_count_done = '1' then
          change_volume;
        else
        end if;
      end if;
    end if;
  end process change_volume;

  volume <= volume_int;
end rtl;

