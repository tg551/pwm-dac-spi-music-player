--
--      Package File Template
--
--      Purpose: This package defines supplemental types, subtypes, 
--               constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package ws2812_data is

  type ws2812_t is
  record
    green : integer range 0 to 255;
    red   : integer range 0 to 255;
    blue  : integer range 0 to 255;
  end record;

  procedure set_ws2812(
    signal w : inout ws2812_t;
    g        : in    integer range 0 to 255;
    r        : in    integer range 0 to 255;
    b        : in    integer range 0 to 255
    );

  procedure clear_ws2812(signal w : inout ws2812_t);

end ws2812_data;

package body ws2812_data is
  procedure set_ws2812(
    signal w : inout ws2812_t;
    g        : in    integer range 0 to 255;
    r        : in    integer range 0 to 255;
    b        : in    integer range 0 to 255
    ) is
  begin
    w.green <= g;
    w.red   <= r;
    w.blue  <= b;
  end set_ws2812;

  procedure clear_ws2812(signal w : inout ws2812_t) is
  begin
    w.green <= 0;
    w.red   <= 0;
    w.blue  <= 0;
  end clear_ws2812;
  
end ws2812_data;
