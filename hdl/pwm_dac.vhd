library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.NUMERIC_STD.all;

use work.fpga_constants.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pwm_dac is
  port(
    clk     : in std_logic;
    reset_n : in std_logic;
    data    : in std_logic_vector(7 downto 0);
    volume  : in natural range 0 to 255;

    req_new_data : out std_logic;
    pwm_pcm_out  : out std_logic
    );
end pwm_dac;
--------------------------------------------------------------------------------------------------


architecture rtl of pwm_dac is
  signal counter_limit  : integer range 0 to (2**8) - 1;
  signal counter        : integer range 0 to (2**8) - 1;
  signal counter_is_max : std_logic;


  ----------------------------------------------------------------------------------------------
  -- sample rate counter
  ----------------------------------------------------------------------------------------------
  constant sample_rate               : positive := 44100;
  constant sample_rate_time          : time     := 1 sec / sample_rate;
  constant sample_rate_counter_limit : positive := sample_rate_time / fpga_clock_period;
  signal sample_rate_counter         : natural range 0 to sample_rate_counter_limit;
  signal sample_rate_counter_done    : std_logic;
begin

  counter_is_max <= '1' when counter >= (2 ** 8) - 1 else '0';

  run_counter : process (clk) is
  begin
    if rising_edge(clk) then
      if reset_n = '0' then
        counter <= 0;
      else
        if counter_is_max = '1' then
          counter <= 0;
        else
          counter <= counter + 1;
        end if;
      end if;
    end if;
  end process run_counter;

  pwm : process (clk) is
  begin
    if rising_edge(clk) then
      if reset_n = '0' then
        pwm_pcm_out <= 'Z';
      else
        if counter_limit = 0 then
          pwm_pcm_out <= 'Z';
        elsif counter <= counter_limit then
          pwm_pcm_out <= '1';
        else
          pwm_pcm_out <= 'Z';
        end if;
      end if;
    end if;
  end process pwm;

  set_limit : process (clk) is
    variable pcm_minus_volume : integer range -255 to 255;
  begin
    if rising_edge(clk) then
      if reset_n = '0' then
        counter_limit <= 0;
      else
        if counter_is_max = '1' then
          pcm_minus_volume := to_integer(unsigned(data(7 downto 0))) - (255 - volume);
          if pcm_minus_volume < 0 then
            counter_limit <= 0;
          else
            counter_limit <= pcm_minus_volume;
          end if;
        end if;
      end if;
    end if;
  end process set_limit;

  count_sample_rate : process (clk) is
  begin
    if rising_edge(clk) then
      if reset_n = '0' then
        sample_rate_counter      <= 0;
        sample_rate_counter_done <= '0';
      else
        if sample_rate_counter >= sample_rate_counter_limit then
          sample_rate_counter      <= 0;
          sample_rate_counter_done <= '1';
        else
          sample_rate_counter      <= sample_rate_counter + 1;
          sample_rate_counter_done <= '0';
        end if;
      end if;
    end if;
  end process count_sample_rate;

  req_new_data <= sample_rate_counter_done;
  
end rtl;

