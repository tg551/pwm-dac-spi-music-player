library IEEE;

use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

use work.fpga_constants.all;

entity debounce is
  generic (
    debounce_time : time
    );
  port (
    clk        : in  std_logic;
    reset_n    : in  std_logic;
    in_signal  : in  std_logic;
    out_signal : out std_logic
    );
end debounce;


architecture rtl of debounce is
  constant counter_limit : integer := debounce_time / fpga_clock_period;
  signal counter         : integer range 0 to counter_limit;
  signal in_signal_d1    : std_logic;
begin
  go : process(clk)
  begin
    if rising_edge(clk) then
      if reset_n = '0' then
        counter      <= 0;
        in_signal_d1 <= '0';
        out_signal   <= '0';
      else
        in_signal_d1 <= in_signal;
        if (in_signal xor in_signal_d1) = '1' then
          counter <= 0;
        elsif counter < counter_limit then
          counter <= counter + 1;
        else
          out_signal <= in_signal;
        end if;
      end if;
    end if;
  end process;
end architecture;
