library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
use work.fpga_constants.all;

entity top is
  port(
    clk : in std_logic;

    --buttons and switches
    pb_0 : in std_logic;
    pb_1 : in std_logic;
    sw_0 : in std_logic;
    sw_1 : in std_logic;

    --leds
    led_0 : out std_logic;
    led_1 : out std_logic;

    --spi signals
    sclk : in  std_logic;
    mosi : in  std_logic;
    cs_n : in  std_logic;
    miso : out std_logic;

    --uart
    --pi_to_fpga_pin : in std_logic;

    --pwm output
    pwm_pcm_out : out std_logic;

    --light square output
    light_square_data : out std_logic
    );
end top;

architecture rtl of top is
  signal reset_n : std_logic;

  -- input conditioning signals
  signal sw_0_db : std_logic;
  signal sw_1_db : std_logic;
  signal pb_0_db : std_logic;
  signal pb_1_db : std_logic;

  --pcm data routing
  signal next_pcm_byte : std_logic_vector(7 downto 0);

  --circular buffer signals
  signal enqueue, dequeue : std_logic;
  signal empty, full      : std_logic;
  signal contents_count   : natural range 0 to ram_depth - 1;
  signal request_more     : std_logic;

  signal last_data_was_zeroes : std_logic;

  --spi signals
  signal miso_int      : std_logic;
  signal spi_rxed_data : std_logic_vector(7 downto 0);
  signal spi_new_data  : std_logic;

  -- volume
  signal volume : integer range 0 to 255;
begin

  --clock_multiplier_1 : entity work.clock_multiplier
  --  port map (
  --    clk_50mhz => clk_50mhz,
  --    clk       => clk
  --    );

  reset_proc : process (clk) is
  begin
    if rising_edge(clk) then
      reset_n <= pb_0 or pb_1;
    end if;
  end process reset_proc;

  input_conditioning_1 : entity work.input_conditioning
    port map (
      clk     => clk,
      reset_n => reset_n,
      sw_0    => sw_0,
      sw_1    => sw_1,
      pb_0    => pb_0,
      pb_1    => pb_1,

      sw_0_db => sw_0_db,
      sw_1_db => sw_1_db,
      pb_0_db => pb_0_db,
      pb_1_db => pb_1_db
      );

  spi_rx_1 : entity work.spi_rx
    port map (
      clk      => clk,
      reset_n  => reset_n,
      sclk     => sclk,
      cs_n     => cs_n,
      mosi     => mosi,
      cpol     => 0,
      cpha     => 0,
      data     => spi_rxed_data,
      new_data => spi_new_data
      );

  --uart_rx_1 : entity work.uart_rx
  --  port map (
  --    clk           => clk,
  --    reset_n       => reset_n,
  --    uart_rx       => pi_to_fpga_pin,
  --    new_data      => uart_new_data,
  --    framing_error => open,
  --    data          => uart_rxed_data);

  only_enqueue_useful_stuff : process (clk) is
  begin
    if rising_edge(clk) then
      if reset_n = '0' then
        enqueue              <= '0';
        last_data_was_zeroes <= '0';
      else
        if spi_new_data = '1' then
          if spi_rxed_data /= "00000000" then
            enqueue              <= '1';
            last_data_was_zeroes <= '1';
          else
            last_data_was_zeroes <= '0';
            enqueue              <= '0';
          end if;
        else
          enqueue <= '0';
        end if;
      end if;
    end if;
  end process only_enqueue_useful_stuff;


  circular_queue_1 : entity work.circular_queue
    port map (
      clk            => clk,
      reset_n        => reset_n,
      enqueue        => enqueue,
      dequeue        => dequeue,
      write_in_data  => spi_rxed_data,
      read_out_data  => next_pcm_byte,
      request_more   => request_more,
      empty          => empty,
      full           => full,
      contents_count => contents_count);
  miso <= request_more;

  volume_control_1 : entity work.volume_control
    port map (
      clk         => clk,
      reset_n     => reset_n,
      volume_up   => pb_0_db,
      volume_down => pb_1_db,
      volume      => volume);

  pwm_dac_1 : entity work.pwm_dac
    port map (
      clk     => clk,
      reset_n => reset_n,
      data    => next_pcm_byte,
      volume  => volume,

      req_new_data => dequeue,
      pwm_pcm_out  => pwm_pcm_out);

  ws2812_debugger_1 : entity work.ws2812_debugger
    port map (
      clk                  => clk,
      reset_n              => reset_n,
      empty                => empty,
      full                 => full,
      request_more         => request_more,
      pcm                  => next_pcm_byte,
      last_data_was_zeroes => last_data_was_zeroes,
      contents_count       => contents_count,

      volume => volume,

      light_square_data => light_square_data);

  led_0 <= full;
  led_1 <= empty;

end rtl;
