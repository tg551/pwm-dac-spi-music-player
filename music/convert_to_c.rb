the_wav = File.binread("wav.wav")[44..-1]

puts "static uint8_t the_music[] = {"

c_formatted_data = the_wav.each_byte.map do |byte|
  "    0x#{byte.to_s 16},"
end.join("\n")[0..-2]

puts c_formatted_data
puts "};\n"
