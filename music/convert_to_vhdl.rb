puts File.read "template/header1.txt"

the_wav = File.binread("wav.wav")[44..-1][0..(64000-1)]

puts File.read("template/header2.txt")

vhdl_formatted_data = the_wav.each_byte.map do |byte|
  "    x\"#{byte.to_s 16}\","
end.join("\n")[0..-2]

puts vhdl_formatted_data
puts

puts File.read "template/footer.txt"
