quietly set PROJECT_DIR "//fs/share/xilinx/projects/resistor-ladder-dac-spi-music-player"

file delete -force presynth 
vlib presynth

vmap presynth presynth

vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/fpga_constants.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/debounce.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/music_ram.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/circular_queue.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/input_conditioning.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/spi_rx.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/uart_rx.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/pwm_dac.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/volume_control.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/ws2812_data.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/ws2812_drv.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/debug_led_fullness.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/debug_volume_led.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/light_square_debugger.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/top.vhd"


vcom -explicit -2008 -work presynth "${PROJECT_DIR}/tb/mock_spi_master.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/tb/uart_functions.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/tb/top_tb.vhd"

vsim -L presynth  -t 1ps presynth.tb


add wave -group clkrst -label clk   /tb/clk
add wave -group clkrst -label rst_n /tb/uut/reset_n

#add wave -group mock -label m_freq  /tb/mock_spi/frequency
#add wave -group mock -label m_send  /tb/mock_spi/send
#add wave -group mock -label m_ready /tb/mock_spi/ready
#add wave -group mock -label m_data  /tb/mock_spi/data
#add wave -group mock -label m_cs_n  /tb/mock_spi/cs_n
#add wave -group mock -label m_sclk  /tb/mock_spi/sclk
#add wave -group mock -label m_mosi  /tb/mock_spi/mosi


#add wave -group spi_rx -label data      /tb/uut/spi_rx_1/data
#add wave -group spi_rx -label new_data  /tb/uut/spi_rx_1/new_data
#add wave -group spi_rx -label bit_index /tb/uut/spi_rx_1/bit_index


add wave -group queue -label enqueue                  /tb/uut/circular_queue_1/enqueue 
add wave -group queue -label dequeue                  /tb/uut/circular_queue_1/dequeue 
add wave -group queue -label r_addr   -radix unsigned /tb/uut/circular_queue_1/read_address
add wave -group queue -label w_addr   -radix unsigned /tb/uut/circular_queue_1/write_address
add wave -group queue -label wrin                     /tb/uut/circular_queue_1/write_in_data 
add wave -group queue -label rout                     /tb/uut/circular_queue_1/read_out_data 
add wave -group queue -label request                  /tb/uut/circular_queue_1/request_more  
add wave -group queue -label full                     /tb/uut/circular_queue_1/full
add wave -group queue -label contents -radix unsigned /tb/uut/circular_queue_1/contents_count


#add wave -group top -label miso  /tb/miso
#add wave -group top -label led_0 /tb/led_0 
#add wave -group top -label led_1 /tb/led_1

#add wave -group pwm_dac -label data           /tb/uut/pwm_dac_2/data 
#add wave -group pwm_dac -label pwm_pcm_out    /tb/uut/pwm_dac_2/pwm_pcm_out 
#add wave -group pwm_dac -label counter_limit  /tb/uut/pwm_dac_2/counter_limit 
#add wave -group pwm_dac -label counter        /tb/uut/pwm_dac_2/counter 
#add wave -group pwm_dac -label counter_is_max /tb/uut/pwm_dac_2/counter_is_max

add wave -group volume -label vol_up        /tb/uut/volume_control_1/volume_up 
add wave -group volume -label vol_down      /tb/uut/volume_control_1/volume_down 
add wave -group volume -label vol           /tb/uut/volume_control_1/volume 
add wave -group volume -label vol_cnt       /tb/uut/volume_control_1/single_volume_counter 
add wave -group volume -label vol_cnt_done  /tb/uut/volume_control_1/single_volume_count_done

change sim:/tb/uut/volume_control_1/single_volume_count 10000

run 1 sec;
wave zoom full
